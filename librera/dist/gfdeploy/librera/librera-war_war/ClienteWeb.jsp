<%-- 
    Document   : ClienteWeb
    Created on : 8/11/2014, 07:19:00 PM
    Author     : Luz Rebollo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="entidad.*, stateless.*, java.math.BigDecimal, javax.naming.*, java.util.*"%>

<!DOCTYPE html>

<%!
    private LibroBeanRemote librocat = null;
    String s1, s2, s3;
    Collection list;

    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando Catalogo Bean" + librocat);
        } catch (Exception ex) {
            System.out.println("error " + ex.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s1 = request.getParameter("titulo");
        s2 = request.getParameter("autor");
        s3 = request.getParameter("precio");
        if (s1 != null && s2 != null && s2 != null) {
            Double precio = new Double(s3);
            BigDecimal b = new BigDecimal(precio);
            librocat.AñadeLibros(s1, s2, b);
            System.out.println("Libro Registrado");
%>
<p>Registro dado de alta</p>  
    

<%
    }
    list = librocat.getLibro();
    for (Iterator iter = list.iterator(); iter.hasNext();) {
        Libro elemento = (Libro) iter.next();
%>



    <br>ID:<%=elemento.getId()%></br>
    <br>TITULO: <%=elemento.getTitulo()%></br>
    <br>AUTOR: <%=elemento.getAutor()%></br>
    <br>PRECIO: <%=elemento.getPrecio()%></br>

<%
    }
    response.flushBuffer();
%>

<a href="index.html">Para Regresar</a>      
<%
    } catch (Exception ex) {
        ex.printStackTrace();
    }
%>